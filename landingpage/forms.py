from django import forms
from .models import Post

class AddPost(forms.ModelForm):
    content = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))

    class Meta:
        model = Post
        fields = '__all__'
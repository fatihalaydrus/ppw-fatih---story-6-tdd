from django.db import models

# Create your models here.
class Post(models.Model):
    content = models.CharField(max_length=300)
    post_time = models.DateTimeField(auto_now_add=True)
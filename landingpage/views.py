from django.shortcuts import render, redirect
from .forms import AddPost
from .models import Post

# Create your views here.
def index(request):
    form = AddPost(request.GET or None)
    if request.method == "GET" and form.is_valid():
            form.save()
            return redirect('landingpage')

    response = {
        'post_form' : form,
        'posts' : reversed(Post.objects.order_by("post_time")) if Post.objects.all() else None,
    }
    return render(request, 'landingpage.html', response)
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .apps import LandingpageConfig
from .views import index
from .models import Post
from .forms import AddPost

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):
    def test_landingpage_app(self):
        self.assertEqual(LandingpageConfig.name, 'landingpage')
        self.assertEqual(apps.get_app_config('landingpage').name, 'landingpage')
    
    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)
    
    def test_landingpage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_landingpage_using_landingpage_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage.html')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_new_post_object(self):
        post = Post.objects.create(content='example')
        self.assertTrue(isinstance(post, Post))
        self.assertEqual(post.content, 'example')

    def test_new_form_object(self):
        form = AddPost()
        self.assertTrue(isinstance(form, AddPost))
    
    def test_invalid_form_data(self):
        form = AddPost(data={"content":301 * "X"})
        self.assertFalse(form.is_valid())

    def test_posts_shown(self):
        post = Post.objects.create(content='example')
        post.save()
        response = Client().get('/')
        self.assertContains(response, ':')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # For local test
        # self.selenium = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)  
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_post(self):
        selenium = self.selenium
        # find the form element
        content = selenium.find_element_by_id('id_content')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        content.send_keys('Automated post')
        # submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn('Automated post', selenium.page_source)

    
    